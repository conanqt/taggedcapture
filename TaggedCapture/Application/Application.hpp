#pragma once

#include <QObject>
#include <span>

#include "Utils/OwnQObjectPointer.hpp"

class QGuiApplication;
class QTranslator;
class QQmlApplicationEngine;

namespace application
{
class ApplicationController;

class Application : public QObject
{
    Q_OBJECT

  public:
    explicit Application(QObject* parent = nullptr);
    ~Application() override;

    int executeApplication(std::span<char*> args);

  private slots:
    static void _onQmlLoaded(const QObject* obj, const QUrl& objUrl);

  private:
    Q_DISABLE_COPY_MOVE(Application)

    static void _setupAppEnvironement();
    void _createQApplication(std::span<char*> args);
    static void _createFolders();
    static void _initializeLogger();
    void _applyTranslations();
    void _createApplicationController();
    void _initializeQmlEngine();
    void _cleanInstances();

    std::unique_ptr<QGuiApplication> _qApplication;
    utils::own_qobject<QTranslator> _translator;
    utils::own_qobject<QQmlApplicationEngine> _qmlEngine;
    utils::own_qobject<ApplicationController> _applicationController;
    std::span<char*> _args;
    int _argv { 0 };
};
}    // namespace application
