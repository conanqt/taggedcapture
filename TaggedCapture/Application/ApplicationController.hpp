#pragma once

#include <QObject>
#include <QQmlEngine>

namespace application
{
class ApplicationController : public QObject
{
    Q_OBJECT
    QML_ELEMENT
  public:
    explicit ApplicationController(QObject* parent = nullptr);
    ~ApplicationController() override = default;

    [[nodiscard]] Q_INVOKABLE QString getDataLocation() const;

  private:
    Q_DISABLE_COPY_MOVE(ApplicationController)
};

}    // namespace application
