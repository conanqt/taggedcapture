#include "ApplicationController.hpp"

#include "Utils/AppFileSystem.hpp"

namespace application
{
ApplicationController::ApplicationController(QObject* parent) : QObject { parent }
{
}

QString ApplicationController::getDataLocation() const
{
    return utils::AppFileSystem::dataFolder();
}

}    // namespace application
