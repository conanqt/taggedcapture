#include "Application.hpp"

#include <QApplication>
#include <QQuickStyle>
#include <QTranslator>
#include <QtQml/QQmlApplicationEngine>

#if QT_CONFIG(permissions)
#include <QPermission>
#endif

#include "ApplicationController.hpp"
#include "Constants.hpp"
#include "Logger/Logger.hpp"
#include "Utils/AppFileSystem.hpp"

namespace application
{
inline const QUrl MAIN_QML_URL = u"qrc:/qt/qml/TaggedCapture/Application/main.qml"_qs;

Application::Application(QObject* parent) : QObject { parent }
{
}

Application::~Application()
{
    qCInfo(applicationCategory) << "Close application";
    _cleanInstances();
    qCInfo(applicationCategory) << "Application closed";
}

int Application::executeApplication(std::span<char*> args)
{
    _setupAppEnvironement();

    _cleanInstances();

    qCInfo(applicationCategory) << "Create application";

    _createQApplication(args);
    _applyTranslations();

    qCInfo(applicationCategory) << "Initialize folders and logger";

    _createFolders();
    _initializeLogger();

    qCInfo(applicationCategory) << "Initialize application controller";

    _createApplicationController();

    qCInfo(applicationCategory) << "Startup of the application";

    _initializeQmlEngine();

    return QGuiApplication::exec();
}

void Application::_onQmlLoaded(const QObject* obj, const QUrl& objUrl)
{
    if (!obj && MAIN_QML_URL == objUrl) QCoreApplication::exit(-1);
}

void Application::_setupAppEnvironement()
{
    QCoreApplication::setApplicationName(u"TaggedCapture"_qs);

    if (qEnvironmentVariable("QT_LOGGING_CONF").isEmpty())
    {
        qputenv("QT_LOGGING_CONF",
                qUtf8Printable(utils::AppFileSystem::sourceApplicationFolder() + "/tclogging.ini"));
    }
}

void Application::_createQApplication(std::span<char*> args)
{
    _args         = args;
    _argv         = static_cast<int>(_args.size());
    _qApplication = std::make_unique<QApplication>(_argv, args.data());
}

void Application::_createFolders()
{
    utils::AppFileSystem::createFolders();
}

void Application::_initializeLogger()
{
    logger::cleanLogger(QDir(utils::AppFileSystem::logFolder()));

    logger::setLogFolder(QDir(utils::AppFileSystem::logFolder()));
    qInstallMessageHandler(logger::messageHandler);
}

void Application::_applyTranslations()
{
    Q_ASSERT(_qApplication);

    _translator                   = utils::make_qobject<QTranslator>(*this);
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString& locale : uiLanguages)
    {
        const QString baseName = QCoreApplication::applicationName() + "_" + QLocale(locale).name();
        if (_translator->load(":/i18n/" + baseName))
        {
            if (QGuiApplication::installTranslator(_translator.get()))
            {
                qCInfo(applicationCategory) << "Successfully installed translation " << baseName;
            }
            else { qCWarning(applicationCategory) << "Failed to install translation " << baseName; }
            break;
        }
        else { qCWarning(applicationCategory) << "Failed to load translation file " << baseName; }
    }
}

void Application::_createApplicationController()
{
    _applicationController = utils::make_qobject<ApplicationController>(*this);
}

void Application::_initializeQmlEngine()
{
    _qmlEngine = utils::make_qobject<QQmlApplicationEngine>(*this);

    _qmlEngine->setInitialProperties(
      { { QStringLiteral(u"applicationController"), QVariant::fromValue(_applicationController.get()) } });

    QObject::connect(_qmlEngine.get(),
                     &QQmlApplicationEngine::objectCreated,
                     this,
                     &Application::_onQmlLoaded,
                     Qt::QueuedConnection);

#if QT_CONFIG(permissions)
    QCameraPermission cameraPermission;
    qApp->requestPermission(cameraPermission,
                            [this](const QPermission& permission)
                            {
                                // Show UI in any case. If there is no permission, the UI will just
                                // be disabled.
                                if (permission.status() != Qt::PermissionStatus::Granted)
                                {
                                    qWarning("Camera permission is not granted! Camera will not be available.");
                                }

                                _qmlEngine->load(MAIN_QML_URL);
                            });
#else
    _qmlEngine->load(MAIN_QML_URL);
#endif
}

void Application::_cleanInstances()
{
    if (_qmlEngine) { _qmlEngine.reset(); }

    if (_translator) { _translator.reset(); }
}
}    // namespace application
