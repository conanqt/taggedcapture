#include "Application/Application.hpp"
#include "import_qml_plugins.h"
#include "qml/src/app_environment.h"

int main(int argc, char* argv[])
{
    set_qt_environment();

    application::Application application;
    return application.executeApplication(std::span { argv, static_cast<std::span<char*>::size_type>(argc) });
}
