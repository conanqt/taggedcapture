cmake_minimum_required(VERSION 3.20)

function(correct_qml_rpath ROOT_DIR)
	file(GLOB_RECURSE SO_FILES_LIST "${ROOT_DIR}/*.so")

	foreach(SO_FILE ${SO_FILES_LIST})
		cmake_path(GET SO_FILE PARENT_PATH SO_PARENT_DIR)

		file(RELATIVE_PATH RELATIVE_PATH_TO_INSTALL_PREFIX "${SO_PARENT_DIR}" "${CMAKE_INSTALL_PREFIX}")

		message("Relative path from ${SO_PARENT_DIR} to ${CMAKE_INSTALL_PREFIX}: ${RELATIVE_PATH_TO_INSTALL_PREFIX}")

		message("Set runtime path of ${SO_FILE} to $ORIGIN/${RELATIVE_PATH_TO_INSTALL_PREFIX}lib")
		execute_process(COMMAND patchelf --set-rpath $ORIGIN/${RELATIVE_PATH_TO_INSTALL_PREFIX}lib ${SO_FILE})
	endforeach()
endfunction()

correct_qml_rpath("${CMAKE_INSTALL_PREFIX}/qml/Qt")
correct_qml_rpath("${CMAKE_INSTALL_PREFIX}/qml/QtQml")
correct_qml_rpath("${CMAKE_INSTALL_PREFIX}/qml/QtQuick")
