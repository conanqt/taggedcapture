#include "LogFile.hpp"

#include <QCoreApplication>

using namespace logger;

LogFile::LogFile(const QString& fileName, QObject* parent) : QFile(fileName, parent)
{
    if (exists())
    {
        // The format of the file is <applicationName>_YYYYMMDD.log
        QString logFileName(QFileInfo(fileName).baseName());
        QString logDate(logFileName.remove(QCoreApplication::applicationName() + "_"));
        _logDate = QDateTime::fromString(logDate, LOG_DATE_FORMAT);
    }
}

LogFile::LogFile(const QDir& path, QObject* parent) : QFile(parent)
{
    QString logFileName;
    logFileName =
      path.path() + "/" + QCoreApplication::applicationName() + "_" + _logDate.toString(LOG_DATE_FORMAT) + ".log";
    setFileName(logFileName);
}
