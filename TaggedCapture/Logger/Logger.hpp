#pragma once

#include <QDir>
#include <QMutex>
#include <QtLogging>

namespace logger
{

namespace details
{
void defaultQtMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message);
void printColoredMessageToConsole(QtMsgType type, const QString& message);
void printMessageToLogFile(const QString& message);
}    // namespace details

void setLogFolder(const QDir& dir);

/*!
 * @brief Delete the too old logs message in the given folder
 * @param dir the folder where the too old log message are cleanned
 */
void cleanLogger(const QDir& dir);

/*!
 * @brief Format and print output log message
 * @param type the type of the log message (debug, warning, critical or fatal)
 * @param context informations about the log message location
 * @param message the message to log
 */
void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message);

}    // namespace logger
