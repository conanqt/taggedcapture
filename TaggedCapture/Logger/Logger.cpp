#include "Logger.hpp"

#include "Constants.hpp"
#include "LogFile.hpp"
#include "LogHandler.hpp"

namespace logger
{
/// The maximal age of a log file in days, older log files than this value are cleaned by the cleanLogger method
constexpr uint LOG_FILE_MAX_AGE = 15;

namespace details
{

QtMessageHandler _defaultQtMessageHandler()
{
    static QtMessageHandler messageHandler = qInstallMessageHandler(nullptr);
    return messageHandler;
}

void defaultQtMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    _defaultQtMessageHandler()(type, context, message);
}

LogHandler* _instance()
{
    // Force initialization to occur only once
    if (static bool initialized = false; !initialized)
    {
        _defaultQtMessageHandler();

#ifdef QT_NO_DEBUG
        qSetMessagePattern("[%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}Warning"
                           "%{endif}%{if-critical}Critical%{endif}%{if-fatal}Fatal%{endif}] (%{category}) %{time"
                           " hh:mm:ss.zzz} : \"%{message}\"");
#else
        qSetMessagePattern("[%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}Warning"
                           "%{endif}%{if-critical}Critical%{endif}%{if-fatal}Fatal%{endif}] (%{category}) %{time"
                           " hh:mm:ss.zzz} : \"%{message}\" %{file}:%{line}");
#endif
        initialized = true;
    }

    static LogHandler instance;
    return &instance;
}

void printMessageToLogFile(const QString& message)
{
    LogHandler* logHandler = _instance();

    if (logHandler->logFile()->open(QIODevice::Append))
    {
        QTextStream(logHandler->logFile()) << message << Qt::endl;
        logHandler->logFile()->close();
    }
    else { qFatal("Cannot write in the log file %s", qUtf8Printable(logHandler->logFile()->fileName())); }
}

}    // namespace details

void setLogFolder(const QDir& dir)
{
    details::_instance()->setLogDir(dir);
}

void cleanLogger(const QDir& dir)
{
    const QStringList filesList(dir.entryList(QStringList() << QStringLiteral("*.log"), QDir::Files));

    for (const QString& logFileName : filesList)
    {
        LogFile logFile(dir.absolutePath() + "/" + logFileName);

        if (logFile.getLogDate().daysTo(QDateTime::currentDateTime()) > LOG_FILE_MAX_AGE)
        {
            qCInfo(loggerCategory) << "Remove log file : " << logFile.fileName();
            logFile.remove();
        }
    }
}

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    static QMutex mutex;
    QMutexLocker lock(&mutex);

    details::defaultQtMessageHandler(type, context, message);

    details::printMessageToLogFile(qFormatLogMessage(type, context, message));
}

}    // namespace logger
