#include "LogHandler.hpp"

#include "LogFile.hpp"

namespace logger
{

LogHandler::LogHandler(const QDir& dir) : _logFile { std::make_unique<LogFile>(dir) }
{
}

LogFile* LogHandler::logFile()
{
    return _logFile.get();
}

void LogHandler::setLogDir(const QDir& dir)
{
    _logFile = std::make_unique<LogFile>(dir);
}

}    // namespace logger
