#pragma once

#include <QDir>

namespace logger
{
class LogFile;

class LogHandler
{
  public:
    LogHandler() = default;
    explicit LogHandler(const QDir& dir);

    LogFile* logFile();
    void setLogDir(const QDir& dir);

  private:
    Q_DISABLE_COPY_MOVE(LogHandler)

    std::unique_ptr<LogFile> _logFile;
};
}    // namespace logger
