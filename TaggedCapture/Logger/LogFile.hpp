#pragma once

#include <QDateTime>
#include <QDir>
#include <QFile>

namespace logger
{

/// The format of the date written in the name of the log file
constexpr QStringView LOG_DATE_FORMAT = u"yyyyMMdd_hhmmsszzz";
/*!
 * @brief Define the name and structure of a log file
 */
class LogFile : public QFile
{
    Q_OBJECT
  public:
    /**
     * @brief Construct a logfile object representing the log file with the given name if the
     * file name is well formatted
     *
     * @param fileName the name of the log file
     * @param parent the object parent of this log file
     */
    explicit LogFile(const QString& fileName, QObject* parent = nullptr);

    /**
     * @brief Create a log file with the current date in the given folder
     * or get the file if it exists
     *
     * @param path the path to the folder where the log file will be created
     * @param parent the object parent of this log file
     */
    explicit LogFile(const QDir& path, QObject* parent = nullptr);

    /*!
     * @brief Get the date at which the log file was generated
     * @return the date at which the log file was generated
     */
    [[nodiscard]] QDateTime getLogDate() const { return _logDate; }

  private:
    /// The date at which the log file was generated
    QDateTime _logDate { QDateTime::currentDateTime() };
};
}    // namespace logger
