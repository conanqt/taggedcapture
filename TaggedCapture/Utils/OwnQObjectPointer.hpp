#pragma once

#include <QObject>
#include <concepts>
#if !defined(QT_NO_DEBUG)
#include <QPointer>
#endif

namespace utils
{

#if !defined(__clang__) || __clang_major__ > 16
template <class T>
concept DerivedQObject = std::is_base_of_v<QObject, T>;
#endif

/*!
 * \brief A owning pointer inspired from unique_ptr but specialized for qobject
 *
 * This object indicates the ownership of a qobject and verify in debug that the qobject have a parent on creation,
 * when it is moved and when the owning object is destroyed
 */
template <class T>
class own_qobject
{
  public:
    constexpr own_qobject() noexcept = default;
    constexpr explicit own_qobject(T* pointer) noexcept
#if !defined(__clang__) || __clang_major__ > 16
        requires DerivedQObject<T>
#endif
        : _pointer { pointer }
    {
        Q_ASSERT(!_pointer || _pointer->parent());
    }
    constexpr ~own_qobject() noexcept { Q_ASSERT(!_pointer || _pointer->parent()); }

    constexpr own_qobject(const own_qobject& other) noexcept = delete;
    constexpr own_qobject(own_qobject&& other) noexcept : _pointer(std::move(other._pointer))
    {
        Q_ASSERT(!_pointer || _pointer->parent());
    }
    constexpr own_qobject& operator=(const own_qobject& other) noexcept = delete;
    constexpr own_qobject& operator=(own_qobject&& other) noexcept
    {
        _pointer = std::move(other._pointer);
        Q_ASSERT(!_pointer || _pointer->parent());
        return *this;
    }

    template <class U>
#if !defined(__clang__) || __clang_major__ > 16
        requires std::convertible_to<U*, T*>
#endif
    explicit constexpr own_qobject(own_qobject<U>&& other) noexcept : _pointer { std::move(other).get() }
    {
        Q_ASSERT(!_pointer || _pointer->parent());
    }
    template <class U>
#if !defined(__clang__) || __clang_major__ > 16
        requires std::convertible_to<U*, T*>
#endif
    constexpr own_qobject& operator=(own_qobject<U>&& other) noexcept
    {
        _pointer = std::move(other).get();
        Q_ASSERT(!_pointer || _pointer->parent());
        return *this;
    }

    constexpr own_qobject& operator=(T* pointer) noexcept
#if !defined(__clang__) || __clang_major__ > 16
        requires DerivedQObject<T>
#endif

    {
        Q_ASSERT(!_pointer || _pointer->parent());
        _pointer = pointer;
        Q_ASSERT(!_pointer || _pointer->parent());
        return *this;
    }

#if defined(QT_NO_DEBUG)
    constexpr bool operator==(const own_qobject& other) const = default;
    constexpr bool operator==(const T* const other) const { return _pointer == other; }
#else
    bool operator==(const own_qobject& other) const = default;
    bool operator==(const T* const other) const { return _pointer == other; }
#endif

    constexpr T* get() const { return _pointer; }
    constexpr explicit operator bool() const noexcept { return _pointer; }
    constexpr typename std::add_lvalue_reference_t<T> operator*() const noexcept { return *_pointer; }
    constexpr T* operator->() const noexcept { return _pointer; }
    void reset()
    {
        Q_ASSERT(_pointer);
#if defined(QT_NO_DEBUG)
        delete _pointer;
#else
        delete _pointer.get();
#endif
        _pointer = nullptr;
    }

  private:
// Use a QPointer in debug to ensure the assert can safely check if the pointer is already destroyed (and =
// nullprt) or if it still exists
#if defined(QT_NO_DEBUG)
    T* _pointer { nullptr };
#else
    QPointer<T> _pointer;
#endif
};

#if !defined(__clang__) || __clang_major__ > 16
template <DerivedQObject T>
#else
template <class T>
#endif
own_qobject<T> make_qobject(QObject& parent)
{
    return own_qobject(new T(&parent));
}

#if !defined(__clang__) || __clang_major__ > 16
template <DerivedQObject T, typename... Args>
#else
template <class T, typename... Args>
#endif
own_qobject<T> make_qobject(QObject& parent, Args... args)
{
    return own_qobject(new T(args..., &parent));
}

}    // namespace utils
