#include "AppFileSystem.hpp"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

#include "FileUtils.hpp"

namespace utils
{
QString AppFileSystem::sourceApplicationFolder()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString AppFileSystem::logFolder()
{
    return sourceApplicationFolder() + QStringLiteral("/log/");
}

QString AppFileSystem::dataFolder()
{
    return sourceApplicationFolder() + QStringLiteral("/data/");
}

void AppFileSystem::createFolders()
{
    utils::FileUtils::createFolder(sourceApplicationFolder());
    utils::FileUtils::createFolder(logFolder());
    utils::FileUtils::createFolder(dataFolder());
}
}    // namespace utils
