#include "FileUtils.hpp"

#include <QDebug>
#include <QDir>
#include <QJsonDocument>
#include <QtGui/QFontDatabase>

#include "Constants.hpp"

namespace utils
{
void FileUtils::createFolder(const QString& folderPath)
{
    QDir folder(folderPath);

    if (!folder.exists())
    {
        if (!folder.mkpath(QStringLiteral(".")))
        {
            qCCritical(utilsCategory) << "Cannot create folder in : " << folderPath;
        }
        else { qCInfo(utilsCategory) << "Created folder : " << folderPath; }
    }
    else { qCDebug(utilsCategory) << "Folder already created : " << folderPath; }
}

}    // namespace utils
