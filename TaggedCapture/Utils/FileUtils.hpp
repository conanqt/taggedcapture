#pragma once

#include <QList>
#include <QString>

namespace utils
{
/*!
 * @brief Utilitarian class containing functions linked to file management
 */
class FileUtils
{
  public:
    /*!
     * @brief Create the folder at the given path
     * @param folderPath the path to the folder to create
     */
    static void createFolder(const QString& folderPath);

    /**
     * @brief Load all the fonts (as ttf file) contains in the given folder and in the subfolders it contains
     * @param sourceFolder : the source folder in which the fonts are loaded
     * @return the list of id of the fonts loaded
     */
    static QList<int> loadFonts(const QString& sourceFolder);
};
}    // namespace utils
