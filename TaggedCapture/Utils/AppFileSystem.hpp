#pragma once

#include <QObject>

namespace utils
{
/*!
 * @brief Give access to the file system of the application
 */
class AppFileSystem
{
  public:
    /*!
     * @brief Create all the path to the folders of the application in the app file system
     */
    static void createFolders();

    /*!
     * @brief Get the path to the source folder of the application
     * @return the path to the source folder of the application
     */
    static QString sourceApplicationFolder();

    /*!
     * @brief Get the path to the log folder of the application
     * @return the path to the log folder of the application
     */
    static QString logFolder();

    /*!
     * @brief Get the path to the folder containing the data of the application
     * @return the path to the folder containing the data of the application
     */
    static QString dataFolder();
};

}    // namespace utils
