import json 
import sys

jsonFile=open(sys.argv[1])
jsonData = json.load(jsonFile)

for jsonPackageData in jsonData:
    if sys.argv[2] in jsonPackageData["reference"]:
        print(jsonPackageData["package_folder"])
