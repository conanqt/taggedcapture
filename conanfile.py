from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake, cmake_layout
from conan.tools.scm import Git
from conan.tools.files import update_conandata
from os.path import join
from glob import glob

class TaggedCapture(ConanFile):
    name="taggedcapture"
    package_type="application"
    url="https://gitlab.com/conanqt/taggedcapture"

    license="GPLv3"
    author="Vincent Baijot, vincentbaijot@gmail.com"
    description="An example of mobile application in cpp qt qml"
    topic=("qt","qml","integration")

    settings="os","compiler","build_type","arch"
    short_paths = True
    options={"build_testing" : [True, False]}
    default_options={"build_testing":False}
    
    def requirements(self):
        self.requires("qt/6.5.3@conanqt+conan-qt/stable")
        
        self.requires("libpng/1.6.42@conanqt+conan-qt/stable")
        self.requires("openssl/3.2.1@conanqt+conan-qt/stable")
        self.requires("zlib/1.3.1@conanqt+conan-qt/stable")
        self.requires("openal-soft/1.22.2@conanqt+conan-qt/stable")
        
        if self.settings.os == "Windows":
            self.requires("brotli/1.1.0@conanqt+conan-qt/stable")
            self.requires("bzip2/1.0.8@conanqt+conan-qt/stable")
            self.requires("double-conversion/3.3.0@conanqt+conan-qt/stable")
            self.requires("freetype/2.13.2@conanqt+conan-qt/stable")
            self.requires("glib/2.78.1@conanqt+conan-qt/stable")
            self.requires("harfbuzz/8.3.0@conanqt+conan-qt/stable")
            self.requires("libffi/3.4.4@conanqt+conan-qt/stable")
            self.requires("libgettext/0.22@conanqt+conan-qt/stable")
            self.requires("libiconv/1.17@conanqt+conan-qt/stable")
            self.requires("libpq/15.4@conanqt+conan-qt/stable")
            self.requires("md4c/0.4.8@conanqt+conan-qt/stable")
            self.requires("pcre2/10.42@conanqt+conan-qt/stable")
            self.requires("sqlite3/3.45.0@conanqt+conan-qt/stable")
            self.requires("zlib/1.3.1@conanqt+conan-qt/stable")
        else:
            self.requires("xkbcommon/1.5.0@conanqt+conan-qt/stable")
            self.requires("expat/2.5.0")
        
    def configure(self):
        self.options["qt"].shared = True
        self.options["qt"].qtdeclarative = True
        self.options["qt"].qttranslations = True
        self.options["qt"].qtimageformats = True
        self.options["qt"].qtlanguageserver = True
        self.options["qt"].qtshadertools = True
        self.options["qt"].qtsvg = True
        self.options["qt"].qtmultimedia = True

        if self.options.build_testing:
            print("Build test")
            self.conf.define("tools.build:skip_test", False)
        else:
            print("Skip test")
            self.conf.define("tools.build:skip_test", True)
    
    def build_requirements(self):
        self.tool_requires("ninja/1.11.1")
        self.tool_requires("cmake/3.28.1")
        self.tool_requires("qtinstaller/4.6.1@conanqt+qtinstaller/stable")
        
        if self.settings.os == "Linux":
            self.tool_requires("linuxdeployqt/10@conanqt+linuxdeployqt/stable")
    
    def set_version(self):
        git = Git(self,self.recipe_folder)
        # Get the git tag to generate the version only if the tag starts with a number 
        self.version = git.run('describe --tags --dirty --match "[0-9]*"')
        
    def export(self):
        git = Git(self,self.recipe_folder)
        update_conandata(self, {"sources": { self.version : {"commit": git.get_commit()} } })
        
    def source(self):
        git = Git(self)
        git.clone(self.url, target=".")
        version_commit = self.conan_data["sources"][self.version]["commit"]
        git.checkout(commit=version_commit)
    
    def layout(self):
        cmake_layout(self)
        
    def generate(self):
        tc = CMakeToolchain(self, generator="Ninja")
        tc.cache_variables["QT_QMAKE_EXECUTABLE"]=join(self.dependencies["qt"].package_folder, "bin", "qmake")
        tc.generate()
        
        cmake = CMakeDeps(self)
        cmake.generate()
        
    def build(self):
        cmake=CMake(self)
        cmake.configure()
        cmake.build()
        
    def package(self):
        self.run("cmake --build . --target package", env="conanrun")
        repository_folder=glob(join("_CPack_Packages","*","IFW"))[0]
        self.copy("TaggedCapture*", src=repository_folder)
