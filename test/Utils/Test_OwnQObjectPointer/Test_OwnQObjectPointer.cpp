#include <QPointer>
#include <QTest>
#include <QTimer>

#include "Utils/OwnQObjectPointer.hpp"

class Test_OwnQObjectPointer : public QObject
{
    Q_OBJECT

  private slots:
    void ownPointer();
};

void Test_OwnQObjectPointer::ownPointer()
{
    QObject context;

    utils::own_qobject<QTimer> ownObject = utils::make_qobject<QTimer>(context);

    QCOMPARE(ownObject->parent(), &context);
    QCOMPARE(ownObject.get(), &(*ownObject));

    QPointer<QTimer> ownObjectPointer = ownObject.get();
    QVERIFY(ownObjectPointer);

    ownObject.reset();
    QVERIFY(!ownObjectPointer);

    utils::own_qobject<QTimer> ownObject2 = utils::own_qobject<QTimer>(new QTimer(&context));

    QCOMPARE(ownObject2->parent(), &context);

    QPointer<QTimer> ownObjectPointer2 = ownObject2.get();

    ownObject = std::move(ownObject2);

    QCOMPARE(ownObjectPointer2, ownObject.get());
}

QTEST_MAIN(Test_OwnQObjectPointer)

#include "Test_OwnQObjectPointer.moc"
