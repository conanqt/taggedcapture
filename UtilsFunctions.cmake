cmake_minimum_required(VERSION 3.22)

function(print_location)
    file(RELATIVE_PATH
        FILE_RELAT_PATH # Output variable
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}
    )

    message("-- Processing ${FILE_RELAT_PATH}/")
endfunction()
