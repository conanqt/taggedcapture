

import QtQuick
import QtQuick.Controls
import QtMultimedia
import TaggedCaptureQmlImports
import TaggedCapture.Application

Item {
    id: root
    width: Constants.width
    height: Constants.height

    property ApplicationController applicationController

    function capture() {
        imageCapture.captureToFile(root.applicationController.getDataLocation())
    }

    function switchCamera(){
        for(const availableCamera of mediaDevices.videoInputs){
           if((camera.cameraDevice.position === CameraDevice.FrontFace && availableCamera.position === CameraDevice.BackFace)
                   || (camera.cameraDevice.position === CameraDevice.BackFace && availableCamera.position === CameraDevice.FrontFace)){
               camera.cameraDevice = availableCamera
               return
           }
        }
        camera.cameraDevice = mediaDevices.defaultVideoInput
    }

    signal imageSaved(string fileName)

    CaptureSession {
        imageCapture: ImageCapture {
            id: imageCapture

            onImageSaved: (id, filename) => {
                root.imageSaved("file://" + filename)
            }
        }
        camera: Camera {
            id: camera

            active: true
        }

        videoOutput: videoOutput
    }
    VideoOutput {
        id: videoOutput
        anchors.fill: root
    }

    MediaDevices {
        id: mediaDevices
    }
}
