
import QtQuick
import TaggedCapture.Application
import TaggedCaptureQmlImports

Image {
    id: image

    function capture() { image.imageSaved("../mock/assets/moonlandermk1.jpg") }
    function switchCamera () {}

    signal imageSaved(string filename)

    property ApplicationController applicationController

    width: Constants.width
    height: Constants.height
    source: "../../assets/peluche-dinosaure-t-rex-jurassic-world-h25-cm.jpg"
}
