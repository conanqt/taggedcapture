cmake_minimum_required(VERSION 3.21.1)

project(TaggedCaptureQml LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(QT_QML_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/qml)
set(QML_IMPORT_PATH ${QT_QML_OUTPUT_DIRECTORY}
    CACHE STRING "Import paths for Qt Creator's code model"
    FORCE
)

option(LINK_INSIGHT "Link Qt Insight Tracker library" ON)
option(BUILD_QDS_COMPONENTS "Build design studio components" ON)

find_package(Qt6 6.5 REQUIRED COMPONENTS Core Gui Qml Quick Multimedia REQUIRED)

qt_standard_project_setup()

print_location()

qt_add_library(TaggedCaptureQml STATIC)

target_link_libraries(TaggedCaptureQml PRIVATE
    Qt${QT_VERSION_MAJOR}::Core
    Qt${QT_VERSION_MAJOR}::Gui
    Qt${QT_VERSION_MAJOR}::Quick
    Qt${QT_VERSION_MAJOR}::QuickControls2
    Qt${QT_VERSION_MAJOR}::Qml
    Qt${QT_VERSION_MAJOR}::Multimedia
)

if (BUILD_QDS_COMPONENTS)
    include(${CMAKE_CURRENT_SOURCE_DIR}/qmlcomponents)
endif()

qt_add_qml_module(TaggedCaptureQml
    URI "TaggedCaptureQml"
    VERSION 1.0
    RESOURCE_PREFIX "/qt/qml"

    DEPENDENCIES
      QtQuick
      QtQuick.Controls
)

add_subdirectory(content)
add_subdirectory(imports)
add_subdirectory(asset_imports)
add_subdirectory(module)

target_link_libraries(TaggedCaptureQml PUBLIC
    contentplugin
    TaggedCaptureQmlImportsplugin
    TaggedCaptureQmlModuleplugin
    Applicationplugin
)

if (LINK_INSIGHT)
    include(${CMAKE_CURRENT_SOURCE_DIR}/insight)
endif ()

include(GNUInstallDirs)
install(TARGETS TaggedCaptureQml
  BUNDLE DESTINATION .
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)


