

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import TaggedCaptureQmlImports

Rectangle {
    id: root

    width: Constants.width
    height: Constants.footerHeight
    color: "#b3b3b3"

    property int selectedFeature : Constants.CameraScreen

    RowLayout {
        id: rowLayout
        anchors.fill: root
        anchors.margins: 10
        spacing: 10

        RoundButton {
            id: menuListButton

            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            implicitHeight: rowLayout.height
            implicitWidth: implicitHeight

            icon.color: "#000000"
            icon.source: "images/menuList.svg"
            icon.height: implicitHeight - 50
            icon.width: implicitWidth - 50

            display: AbstractButton.IconOnly

            checkable: true
            checked: root.selectedFeature === Constants.MenuListScreen

            Connections {
                target: menuListButton
                onClicked: root.selectedFeature = Constants.MenuListScreen
            }
        }

        RoundButton {
            id: cameraButton
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            implicitHeight: rowLayout.height
            implicitWidth: implicitHeight

            icon.color: "#000000"
            icon.width: implicitWidth - 50
            icon.height: implicitHeight - 50
            icon.source: "images/photo-camera-svgrepo-com.svg"

            display: AbstractButton.IconOnly

            checkable: true
            checked: root.selectedFeature === Constants.CameraScreen

            Connections {
                target: cameraButton
                onClicked: root.selectedFeature = Constants.CameraScreen
            }
        }

        RoundButton {
            id: editButton
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            implicitHeight: rowLayout.height
            implicitWidth: implicitHeight

            icon.color: "#000000"
            icon.width: implicitWidth - 50
            icon.height: implicitWidth - 50
            icon.source: "images/edit-pen-svgrepo-com.svg"

            display: AbstractButton.IconOnly

            checkable: true
            checked: root.selectedFeature === Constants.EditionScreen

            Connections {
                target: editButton
                onClicked: root.selectedFeature = Constants.EditionScreen
            }
        }
    }
}
