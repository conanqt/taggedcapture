// Copyright (C) 2021 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
import QtQuick
import TaggedCaptureQmlImports
import QtQuick.Controls 6.5
import TaggedCapture.Application

Window {
    id: root
    visibility: Window.AutomaticVisibility
    visible: true
    title: "TestAndroid"
    width: Constants.width
    height: Constants.height

    property ApplicationController applicationController

    Item {
        id: mainScreen

        property alias selectedFeature: footer.selectedFeature

        property string savedImage

        anchors.fill: parent

        Loader {
            id: screenLoader

            anchors {
                top: mainScreen.top
                left: mainScreen.left
                right: mainScreen.right
                bottom: footer.top
            }

            sourceComponent: {
                if (mainScreen.selectedFeature === Constants.CameraScreen) {
                    return cameraComponent
                } else if (mainScreen.selectedFeature === Constants.MenuListScreen) {
                    return menuListComponent
                } else {
                    return editionComponent
                }
            }
        }

        Component {
            id: cameraComponent
            CameraCaptureScreen {
                id: cameraCaptureScreen

                applicationController: root.applicationController

                onImageSaved: (imagePath) => {
                    mainScreen.selectedFeature = Constants.EditionScreen
                    mainScreen.savedImage = imagePath
                }
            }
        }

        Component {
            id: menuListComponent
            Rectangle {
                color: "red"
            }
        }

        Component {
            id: editionComponent
            EditionScreen {
                id: editionScreen

                source: mainScreen.savedImage
            }
        }

        FooterMenu {
            id: footer

            height: Constants.footerHeight
            anchors {
                bottom: mainScreen.bottom
                left: mainScreen.left
                right: mainScreen.right
            }
        }
    }
}
