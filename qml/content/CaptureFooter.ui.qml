

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import TaggedCaptureQmlImports

RowLayout {
    id: rowLayout

    width: Constants.width
    height: Constants.footerHeight

    spacing: 10

    signal captureButtonClicked
    signal switchButtonClicked

    RoundButton {
        id: videoButton
        text: "Video"
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        Layout.fillHeight: true
        Layout.fillWidth: false
        flat: false

        implicitHeight: rowLayout.height
        implicitWidth: implicitHeight

        display: AbstractButton.TextOnly
    }

    RoundButton {
        id: cameraButton
        text: "Capture"
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        implicitHeight: rowLayout.height
        implicitWidth: implicitHeight

        display: AbstractButton.TextOnly

        Connections {
            target: cameraButton
            onClicked: rowLayout.captureButtonClicked()
        }
    }

    RoundButton {
        id: switchButton
        text: "Switch"
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        implicitHeight: rowLayout.height
        implicitWidth: implicitHeight

        display: AbstractButton.TextOnly

        Connections {
            target: switchButton
            onClicked: rowLayout.switchButtonClicked()
        }
    }
}
