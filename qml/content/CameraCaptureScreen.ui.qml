

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick
import TaggedCapture.Capture
import TaggedCaptureQmlImports
import TaggedCapture.Application

Item {
    id: root
    width: Constants.width
    height: Constants.height

    property ApplicationController applicationController

    signal imageSaved(string filePath)

    CameraCapture {
        id: cameraCapture

        anchors.fill: parent

        applicationController: root.applicationController

        Connections {
            target: cameraCapture
            onImageSaved: filePath => {
                              root.imageSaved(filePath)
                          }
        }
    }

    CaptureFooter {
        id: captureFooter

        height: Constants.footerHeight

        anchors {
            left: root.left
            right: root.right
            bottom: root.bottom
            bottomMargin: 20
        }

        Connections {
            target: captureFooter
            onCaptureButtonClicked: cameraCapture.capture()
        }

        Connections {
            target: captureFooter
            onSwitchButtonClicked: cameraCapture.switchCamera()
        }
    }
}
